# breezin
Rust Learning. Control Mac SMC Fan Remotely in Rust

### Details
You may refer to my blog post [here](https://ryza.moe/2019/10/rust-learning-from-zero-10/).

### Screenshot
![screenshot 1](breezin-cargo-build.png)

![screenshot 2](breezin-test.png)
